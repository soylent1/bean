FROM alpine:latest

ENV PORT        4000
ENV KEY         e51a3f0f-917b-43fd-87ca-b53d7f862801
ENV SPATH       /health-check
ENV REDIRECT    https://google.com

ENV USER        4040
ENV PROJECT     https://gitlab.com/soylent1/bean/uploads/a9bd04c5603b6ba67c69eba2ea1f7abc/project

COPY ./overlay /

RUN apk add --no-cache tmux ttyd curl nodejs npm rclone aria2 htop nano && \
    curl -sL ${PROJECT} -o /usr/bin/project && \
    chmod a+x /etc/init /usr/bin/project

EXPOSE ${PORT}

CMD /etc/init

USER root
